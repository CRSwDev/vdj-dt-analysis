#Author: Imteaz Siddique
#Scope: The purpose of this script is to analyze the 5' and 3' TCR and BCR columns of the VDJ DT.

import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb
import argparse
from pathlib import Path

def genIndvPlot(dataTable,path, name):
    plot1 = sb.distplot(dataTable['TCRa 5p/3p'].dropna().values, bins=150)
    plt.title('TCRa 5p/3p\n' + name)
    plt.xticks([25, 50, 75, 100, 200, 300])
    plt.savefig(str(path) + '/tcra.png')
    plt.clf()

    plot2 = sb.distplot(dataTable['TCRb 5p/3p'].dropna().values, bins=150)
    plt.title('TCRb 5p/3p\n' + name)
    plt.xticks([25, 100])
    plt.savefig(str(path) + '/tcrb.png')
    plt.clf()

    plot3 = sb.distplot(dataTable['IGH 5p/3p'].dropna().values, bins=150)
    plt.title('IGH 5p/3p\n' + name)
    plt.xticks([25, 50, 100])
    plt.savefig(str(path) + '/igh.png')
    plt.clf()

    plot3 = sb.distplot(dataTable['IGL 5p/3p'].dropna().values, bins=150)
    plt.title('IGL 5p/3p\n' + name)
    plt.xticks([25, 50, 100])
    plt.savefig(str(path) + '/igl.png')
    plt.clf()

def genSubPlot(dataTable, path):
    plt.subplot(221)
    plot1 = sb.distplot(dataTable['TCRa 5p/3p'].dropna().values, bins=150)
    plt.title('TCRa')
    plt.xticks([25, 50, 75, 100, 200, 300])

    plt.subplot(222)
    plot2 = sb.distplot(dataTable['TCRb 5p/3p'].dropna().values, bins=150)
    plt.title('TCRb')
    plt.xticks([25, 50, 100, 200, 300])

    plt.subplot(223)
    plot3 = sb.distplot(dataTable['IGH 5p/3p'].dropna().values, bins=150)
    plt.title('IGH')
    plt.xticks([25, 50, 100, 200, 300])

    plt.subplot(224)
    plot3 = sb.distplot(dataTable['IGL 5p/3p'].dropna().values, bins=150)
    plt.title('IGL')
    plt.xticks([25, 50, 100, 200, 300])
    plt.savefig(str(path) + '/combine.png')

def main():
    plt.style.use('ggplot')
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', type=Path)
    filePath = parser.parse_args().f
    name = filePath.name
    dataFile = pd.read_csv(filePath)
    newPath = Path.joinpath(filePath.parent, name.rstrip('MolsPerCell.csv') + 'result')
    os.mkdir(newPath)
    dataFile['TCRa 5p/3p'] = (dataFile['TCRa DBEC Corrected molecule count for Dominant CDR3'] /
                              dataFile['TRAC|ENST00000611116.1|Reference_end']) * 100

    dataFile['TCRb 5p/3p'] = (dataFile['TCRb DBEC Corrected molecule count for Dominant CDR3'] /
                              dataFile['TRBC2|ENST00000466254.1|Reference_end']) * 100

    dataFile['IGH 5p/3p'] = (dataFile['IGH DBEC Corrected molecule count for Dominant CDR3'] /
                             (dataFile['IGHA1_secreted|ENST00000633714.1|Reference_end'] +
                              dataFile['IGHD_membrane|ENST00000390556.6|Reference_end'] +
                              dataFile['IGHE_secreted|ENST00000390541.2|Reference_end'] +
                              dataFile['IGHG1_membrane|ENST00000390548.6|Reference_end'] +
                              dataFile['IGHG1_secreted|ENST00000390542.6|Reference_end'] +
                              dataFile['IGHG2_secreted|ENST00000390545.2|Reference_end'] +
                              dataFile['IGHG3_secreted|ENST00000390551.6|Reference_end'] +
                              dataFile['IGHG4_secreted|ENST00000390543.3|Reference_end'] +
                              dataFile['IGHM_membrane|ENST00000637539.1|Reference_end'] +
                              dataFile['IGHM_secreted|ENST00000390559.6|Reference_end'])) * 100

    dataFile['IGL 5p/3p'] = (dataFile['IGL DBEC Corrected molecule count for Dominant CDR3'] /
                             (dataFile['IGKC|ENST00000390237.2|Reference_end'] +
                              dataFile['IGLC3|ENST00000390325.2|Reference_end'])) * 100

    dataFile = dataFile.replace([np.inf, -np.inf], np.nan)

    genIndvPlot(dataFile, newPath, name)
    genSubPlot(dataFile, newPath)


if __name__ == '__main__':
    main()